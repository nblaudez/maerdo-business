import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaerdoProspectModule } from '../maerdo-prospect/maerdo-prospect.module';
import { MaerdoUserModule } from '../maerdo-user/maerdo-user.module';

@NgModule({
  imports: [
    CommonModule,
    MaerdoUserModule
  ],
  declarations: []
})
export class MaerdoBusinessModule { }
