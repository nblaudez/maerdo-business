import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaerdoBusinessComponent } from './maerdo-business.component';

describe('MaerdoBusinessComponent', () => {
  let component: MaerdoBusinessComponent;
  let fixture: ComponentFixture<MaerdoBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaerdoBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaerdoBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
